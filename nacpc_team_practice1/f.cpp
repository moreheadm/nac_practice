
#include <bits/stdc++.h>

using namespace std;
int maxn = int(1e5) + 5;
int parent[maxn];
int depth[maxn];
bool visited[maxn];
int n;
vector<int> adjacent[maxn];
bool winning_subtree[maxn];
bool winning[maxn];

void dfs(int v, int d) {
    depth[v] = d;
    for (int u : adjacent[v]) {
        if (visited[u]) continue;
        visited[u] = true;
        parent[u] = v;
        dfs(u, d);
    }
}

bool dfs2(int v) {
    set<int> winning_children;
    for (int u : adjacent[v]) {
        if (visited[u]) continue;
        visited[u] = true;
        bool is_winning_subtree = dfs2(u);
        if (is_winning_subtree) {
            winning_children.insert(u);
        }
    }

    winning_subtree[v] = (winning_children.size() == 0);
    return winning_subtree[v];
}


int main() {
    scanf("%d", &n);
    for (int i = 0; i < n - 1; i++) {
        int u, v;
        scanf("%d %d", &u, &v);
        adjacent[u].push_back(v);
        adjacent[v].push_back(u);
    }

    fill(visited, visited + n, false);
    visited[0] = true;
    dfs(0, 0);
}
