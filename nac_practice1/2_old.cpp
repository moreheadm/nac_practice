
#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

const int INF = int(1e8);

int const maxm = (1 << 11) + 16;
int const maxn = maxm;
int const num_nodes = 2 + maxn + 2 * maxn + maxm;

int n, m;
typedef pair<int, int> nwp;

vector<int> nodes[num_nodes];
vector<int> tempNodes[num_nodes];
int weights[num_nodes][num_nodes];

bool visited[num_nodes];

int key_idx(int key) {
    return key + 2 + n;
}

int pair_idx(int pair) {
    return pair + 2;
}

int door_idx(int door) {
    return door + 2 + n + 2 * n;
}

bool dfs(int s, int t, vector<int> & path) {
    for (int v : tempNodes[s]) {
        if (visited[v]) continue;
        if (weights[s][v] >= 1) continue;
        visited[v] = true;

        path.push_back(v);
        if (v == t) { return true; }

        bool isPath = dfs(v, t, path);
        if (isPath) { return true; }
        path.pop_back();
    }
    return false;
}

pair<int, int> key_pairs[maxn];
pair<int, int> door_pairs[maxm];

bool solve() {
    scanf("%d%d", &n, &m);
    if (n == 0 && m == 0) {
        return false;
    }
    for (int i = 0; i < n; i++) {
        int key1, key2;
        scanf("%d%d", &key1, &key2);
        key_pairs[i] = {key1, key2};

        /*nodes[pair_idx(i)].push_back({key_idx(key1), -1});
        nodes[key_idx(key1)].push_back({pair_idx(i), 1});
        nodes[pair_idx(i)].push_back({key_idx(key2), -1});
        nodes[key_idx(key2)].push_back({pair_idx(i), 1});

        nodes[pair_idx(i)].push_back({1, 1});
        nodes[1].push_back({pair_idx(i), -1});
        */

        nodes[pair_idx(i)].push_back(key_idx(key1));
        nodes[key_idx(key1)].push_back(pair_idx(i));
        nodes[pair_idx(i)].push_back(key_idx(key2));
        nodes[key_idx(key2)].push_back(pair_idx(i));

        nodes[pair_idx(i)].push_back(1);
        nodes[1].push_back(pair_idx(i));
    }

    for (int i = 0; i < m; i++) {
        int key1, key2;
        scanf("%d%d", &key1, &key2);
        door_pairs[i] = {key1, key2};
    }

    int lo = 0, hi = m;
    while (true) {
        if (lo >= hi) {
            break;
        }

        int mid = (lo + hi + 1) / 2;
        cerr << "Mid: " << mid << endl;
        int maxFlow = 0;
        for (int i = 0; i < 2 + n + 2 * n + m; i++) {
            tempNodes[i] = nodes[i];
        }

        for (int i = 0; i < num_nodes; i++) {
            visited[i] = false;
            for (int j = 0; j < num_nodes; j++) {
                weights[i][j] = 0;
            }
        }

        for (int i = 0; i < mid; i++) {
            auto p = door_pairs[i];

            tempNodes[door_idx(i)].push_back(key_idx(p.first));
            tempNodes[key_idx(p.first)].push_back(door_idx(i));
            tempNodes[door_idx(i)].push_back(key_idx(p.second));
            tempNodes[key_idx(p.second)].push_back(door_idx(i));

            tempNodes[0].push_back(door_idx(i));
            tempNodes[door_idx(i)].push_back(0);
        }

        while (true) {
            vector<int> path;
            path.push_back(0);
            visited[0] = true;
            bool isPath = dfs(0, 1, path);
            if (!isPath) break;
            for (int i = 0; i < num_nodes; i++) { visited[i] = false; }

            cerr << "Path:";
            for (int i = 0; i < path.size() - 1; i++) {
                cerr << " " << path[i] << "-" << path[i + 1];
                weights[path[i]][path[i + 1]] += 1;
                weights[path[i + 1]][path[i]] -= 1;
            }
            cerr << endl;
            maxFlow++;
        }
        cerr << "Max flow: " << maxFlow << endl;

        if (maxFlow == mid) {
            lo = mid;
        } else {
            hi = mid - 1;
        }
    }
    printf("%d\n", lo);

    return true;
}

int main() {
    while (solve()) { }
}

