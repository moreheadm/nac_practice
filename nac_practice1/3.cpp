
#include<cstdio>
#include<iostream>
using namespace std;
typedef long long ll;


const int maxp = int(1e4) + 5;
int n;
bool board[maxp];


void solve() {

    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        int p;
        scanf("%d", &p);
        board[p - 1] = true;
    }

    int idx = 0;
    for (; idx < maxp; idx++) {
        if (!board[idx]) break;
    }

    if (idx >= maxp) {
        cout << "Bob will win" << endl;
        return;
    }

    bool losing = true;
    bool resultLosing = true;
    int lastIdx = idx;
    for (;idx < maxp; idx++) {
        if (board[idx]) {
            int startIndex = idx;
            for (; idx < maxp; idx++) {
                if(!board[idx]) break;
            }
            int count = idx - startIndex;

            cerr << "Test " << count << endl;
            if (losing && count % 2 == 1) {
                losing = true;
                resultLosing = false;
            } else if (losing && count % 2 == 0) {
                losing = false;
                resultLosing = true;
            } else if (!losing && count % 2 == 1) {
                resultLosing = ((startIndex - lastIdx) % 2 == 0);
                losing = !resultLosing;
            } else {
                resultLosing = ((startIndex - lastIdx) % 2 == 1);
                losing = !resultLosing;
            }

            lastIdx = idx;
        }
    }

    if (resultLosing) {
        cout << "Bob will win\n";
    } else {
        cout << "Georgia will win\n";
    }

    
}

int main() {
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; i++) {
        solve();
    }
}
