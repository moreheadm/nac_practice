

#include <algorithm>
#include <iostream>
#include <set>
#include <vector>
#include <cstdio>
#include <iterator>

using namespace std;
typedef long long ll;

const int maxn = 4 * (1 << 10) + 10;
vector<int> adjacent[maxn];
bool visited[maxn];
int component[maxn];
int n, m;

void dfs(int v, vector<int> & rpo) {
    for (int i = 0; i < adjacent[v].size(); i++) {
        int u = adjacent[v][i];
        if (visited[u]) continue;
        visited[u] = true;
        dfs(u, rpo);
    }
    rpo.push_back(v);
}

bool dfs2(int v, int count) {
    //cerr << v << endl;
    for (int i = 0; i < adjacent[v].size(); i++) {
        int u = adjacent[v][i];
        if (visited[u]) continue;
        visited[u] = true;
        if (component[u ^ 1] == count) return false;
        component[u] = count;
        bool sat = dfs2(u, count);
        if (!sat) return sat;
    }
    //cerr << "End" << endl;
    return true;
}

bool solve_2sat() {
    fill(visited, visited + maxn, false);
    vector<int> rpo;
    for (int i = 0; i < 4 * n; i++) {
        if (visited[i]) continue;
        visited[i] = true;
        dfs(i, rpo);
    }

    fill(visited, visited + maxn, false);
    fill(component, component + maxn, -1);
    for (int i = 0; i < rpo.size(); i++) {
        if (visited[rpo[i]]) continue;
        visited[rpo[i]] = true;
        bool sat = dfs2(rpo[i], i);
        if (!sat) return false;
    }
    return true;
}


bool solve() {
    scanf("%d%d", &n, &m);
    if (n == 0 && m == 0) {
        return false;
    }

    //cout << "Test " << n << " " << m << endl;
    for (int i = 0; i < n; i++) {
        int key1, key2;
        scanf("%d%d", &key1, &key2);
        adjacent[2 * key1].push_back(2 * key2 + 1);
        adjacent[2 * key2].push_back(2 * key1 + 1);
   }

    vector< pair<int, int> > v;
    for (int i = 0; i < m; i++) {
        int key1, key2;
        scanf("%d%d", &key1, &key2);
        v.push_back(make_pair(key1, key2));
    }
    for (int i = 0; i < v.size(); i++) {
        pair<int, int> p = v[i];
        int key1 = p.first, key2 = p.second;
        //cerr << "Key: " << key1 << " " << key2 << endl;
        adjacent[2 * key1 + 1].push_back(2 * key2);
        adjacent[2 * key2 + 1].push_back(2 * key1);
        bool sat = solve_2sat();
        if (!sat) {
            printf("%d\n", i);
            return true;
        }
    }
    printf("%d\n", m);
    return true;
}

int main() {
    while (solve()) { }
}

